Name:		catfish
Version:	4.18.0
Release:	1
Summary:	A handy file search tool
License:	GPLv2+
#Group:          Development/Libraries
URL:		https://docs.xfce.org/apps/catfish/start
Source0:	https://archive.xfce.org/src/apps/catfish/1.4/%{name}-%{version}.tar.bz2
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildArch:	noarch
BuildRequires:	desktop-file-utils
BuildRequires:	gettext
BuildRequires:	intltool
BuildRequires:	python3-devel
BuildRequires:	python3-distutils-extra
BuildRequires:	python3-setuptools
BuildRequires:	%{_bindir}/appstream-util
BuildRequires:	python3-gobject
BuildRequires:	python3-pexpect
BuildRequires:	python3-dbus
Requires:	python3-gobject
Requires:	python3-pexpect
Requires:       python3-dbus
Requires:	xdg-utils
# search engine
Requires:	%{_bindir}/locate

Provides:	%{name}-python3 = %{version}-%{release}
Provides:	%{name}-common = %{version}-%{release}

%description
Catfish is a handy file searching tool. The interface is
intentionally lightweight and simple, using only GTK+3.
You can configure it to your needs by using several command line
options.

%prep
# Because %%build is not allowed to write files under %%buildroot,
# and calling setup.py --installed with target root under unpackaged directory
# causes some trouble, here once create temporaily directory
# and unpack source after that

%setup -q -T -c -a 0 %{name}-%{version}
pushd %{name}-%{version}*

# Fix up permissions...
find . -type f -print0 | xargs --null chmod 0644
chmod 0755 bin/%{name}

popd

%build
TOPDIR=$(pwd)

pushd %{name}-%{version}*

# Remove unneeded shebang
grep -rl "/usr/bin/env" . | \
	xargs sed -i -e "\@/usr/bin/env[ ][ ]*python@d"

#%%py3_build
# separation of build / install --skip-build not supported
# (separation causes some error for creating additional files
#  such as desktop file, also installation directory gets wrong)

mkdir -p ./_TMPINSTDIR/python3
%__python3 setup.py \
	install \
	--root ${TOPDIR}/_TMPINSTDIR/python3

popd

%install
cp -a %{name}-%{version}*/[A-Z]* .

#%%py3_install
cp -a _TMPINSTDIR/python3/* %{buildroot}

# Explicitly set GDK_BACKEND
mkdir %{buildroot}%{_libexecdir}
mv %{buildroot}%{_bindir}/catfish %{buildroot}%{_libexecdir}/
cat > %{buildroot}%{_bindir}/catfish <<EOF
#!/usr/bin/bash

export GDK_BACKEND=x11
exec %{_libexecdir}/catfish \$@
EOF
chmod 0755 %{buildroot}%{_bindir}/catfish

# for backwards compatibility:
ln -s catfish %{buildroot}%{_bindir}/catfish-py3


pushd %{name}-%{version}*

# Install man page manually
%{__mkdir_p} %{buildroot}%{_mandir}/man1
%{__install} -cpm 0644 ./%{name}.1 %{buildroot}%{_mandir}/man1/

popd

# Remove all unnecessary documentation
%{__rm} -rf %{buildroot}%{_datadir}/doc/

# Rename desktop files for now
pushd %{buildroot}%{_datadir}/applications/
mv *desktop %{name}.desktop
popd

%{find_lang} %{name}

%check
desktop-file-validate %{buildroot}%{_datadir}/applications/%{name}.desktop
appstream-util validate-relax --nonet %{buildroot}%{_metainfodir}/%{name}.appdata.xml

%files -f %{name}.lang
%doc AUTHORS
%license COPYING
%{_bindir}/%{name}
%{_bindir}/%{name}-py3
%{_libexecdir}/%{name}
%{_mandir}/man1/%{name}.1*
%{_datadir}/%{name}/
%{_datadir}/applications/%{name}.desktop
%{_metainfodir}/%{name}.appdata.xml
%{python3_sitelib}/%{name}/
%{python3_sitelib}/%{name}_lib/
%{python3_sitelib}/%{name}-%{version}-py3*.egg-info
%{_datadir}/icons/hicolor/

%changelog
* Mon Jan 15 2024 zhangxingrong <zhangxingrong@uniontech.com> - 4.18.0-1
- upgrade to 4.18.0

* Wed Jan 04 2023 misaka00251 <liuxin@iscas.ac.cn> - 4.16.4-1
- Update to 4.16.4

* Thu Jun 23 2022 Dillon Chen <dillon.chen@gmail.com> - 4.16.3-1
- update to 4.16.3

* Tue Jul 28 2020 Dillon Chen <dillon.chen@turbolinux.com.cn> - 1.4.13-1
- Init package
